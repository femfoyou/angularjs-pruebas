// Creación del módulo
var angularRoutingApp = angular.module('angularRoutingApp', ['ngRoute']);

// Configuración de las rutas
angularRoutingApp.config(function($routeProvider,$locationProvider) {

//    for (i=1; i<=1200; i++) {
	$routeProvider
		
        .when('/', {
			templateUrl	: 'pages/home.html',
			controller 	: 'mainController'
		})
		.when('/acerca', {
			templateUrl : 'pages/acerca.html',
			controller 	: 'aboutController'
		})
		.when('/lista', {
			templateUrl : 'pages/lista.html',
			controller 	: 'contactController'
		})
        
//        .when('/page/:pageId', {
//			templateUrl: 'paglista/home' + $routeParams.pageId + '.html',
//			controller: 'mainController'
//		})
    
    
//        .when('/lista6'+i, {
//                templateUrl: 'paglista/home'+i+'.html',
//        })
    
        
//        .when('/lista0/:listaId', {
//            templateUrl: '/lista'+$routeParams.listaId+'.html',
//            controller: 'RouteController'
//        })
        //     .when("/page/6",{
        //         templateUrl : "ext1.html" ,
        //         controller : "ChildCtrl1",
        //         resolve : {
        //         factory : function  ($rootScope,$location) {
        //             if ($rootScope.isCurrentTaskDone) {
        //                 return true;
        //             }
        //             else{
        //                 $location.path('/page/6');
        //                 return false;
        //             }
        //         }
        //     }
        // })
    
/* paginas externas */
    
        .when('/page1/:param', {
			templateUrl : 'paglista/home5.html',
            controller: 'mainController'
		})
//        .when('/lista1', {
//			templateUrl : 'paglista/home1.html',
//		})
//        .when('/lista2', {
//			templateUrl : 'paglista/home2.html',
//		})
//        .when('/lista3', {
//			templateUrl : 'paglista/home3.html',
//		})
//        .when('/lista4', {
//			templateUrl : 'paglista/home4.html',
//		})
//        .when('/lista5', {
//			templateUrl : 'paglista/home5.html',
//	})
       .when('/page2/:ext1', {
			templateUrl : 'ext1.html',
            controller: 'mainController'
		})
       .when('/page8/:capi:versi:sub', {
            templateUrl : 'ext2.html',
            controller: 'mainController'
        })
        
        // #/page8/ext2.html#
    
		.otherwise({
			redirectTo: '/'
		});

//    }
});


angularRoutingApp.controller('mainController', function($scope, $route, $routeParams, $location) {
    
    // tamaño fuente controlador

    $scope.fuente = {
      fontSize : 24,
      style:{},
    };                
    $scope.estiloFont = function(fuente){
      fuente.style = {'font-size':fuente.fontSize+'px'}
    };
    $scope.style = function(fuente) {
      return fuente.style;
    }

    $scope.$route = $route; 
    $scope.$location = $location; 
    $scope.$routeParams = $routeParams;
    

    $scope.params = $routeParams;

    // $routeParams.capi + "/" +
    // $routeParams.versi + "/" +
    // $routeParams.sub;

    
});

angularRoutingApp.controller('aboutController', function($scope) {
    
});

angularRoutingApp.controller('contactController', function($scope) {
    
});