// Creación del módulo
var angularRoutingApp = angular.module('angularRoutingApp', ['ngRoute']);

// Configuración de las rutas
angularRoutingApp.config(function($routeProvider) {

	$routeProvider
		.when('/', {
			templateUrl	: 'pages/inicio.html',
			controller 	: 'mainController'
		})
		.when('/lista', {
			templateUrl : 'pages/lista.html',
			controller 	: 'aboutController'
		})
		.when('/config', {
			templateUrl : 'pages/contacto.html',
			controller 	: 'contactController'
		})
    
/* paginas externas */
    
         .when('/c001', { templateUrl : 'listas/Section0002.html',})
    .when('/c002', { templateUrl : 'listas/Section0003.html',})
    .when('/c003', { templateUrl : 'listas/Section0004.html',})
    .when('/c004', { templateUrl : 'listas/Section0005.html',})
    .when('/c005', { templateUrl : 'listas/Section0006.html',})
    .when('/c006', { templateUrl : 'listas/Section0007.html',})
    .when('/c007', { templateUrl : 'listas/Section0008.html',})
    .when('/c008', { templateUrl : 'listas/Section0009.html',})
    .when('/c009', { templateUrl : 'listas/Section0010.html',})
    .when('/c010', { templateUrl : 'listas/Section0011.html',})

    .when('/c011', { templateUrl : 'listas/Section0012.html',})
    .when('/c012', { templateUrl : 'listas/Section0013.html',})
    .when('/c013', { templateUrl : 'listas/Section0014.html',})
    .when('/c014', { templateUrl : 'listas/Section0015.html',})
    .when('/c015', { templateUrl : 'listas/Section0016.html',})
    .when('/c016', { templateUrl : 'listas/Section0017.html',})
    .when('/c017', { templateUrl : 'listas/Section0018.html',})
    .when('/c018', { templateUrl : 'listas/Section0019.html',})
    .when('/c019', { templateUrl : 'listas/Section0020.html',})
    .when('/c020', { templateUrl : 'listas/Section0021.html',})

    .when('/c021', { templateUrl : 'listas/Section0022.html',})
    .when('/c022', { templateUrl : 'listas/Section0023.html',})
    .when('/c023', { templateUrl : 'listas/Section0024.html',})
    .when('/c024', { templateUrl : 'listas/Section0025.html',})
    .when('/c025', { templateUrl : 'listas/Section0026.html',})
    .when('/c026', { templateUrl : 'listas/Section0027.html',})
    .when('/c027', { templateUrl : 'listas/Section0028.html',})
    .when('/c028', { templateUrl : 'listas/Section0029.html',})
    .when('/c029', { templateUrl : 'listas/Section0030.html',})
    .when('/c030', { templateUrl : 'listas/Section0031.html',})

    .when('/c031', { templateUrl : 'listas/Section0032.html',})
    .when('/c032', { templateUrl : 'listas/Section0033.html',})
    .when('/c033', { templateUrl : 'listas/Section0034.html',})
    .when('/c034', { templateUrl : 'listas/Section0035.html',})
    .when('/c035', { templateUrl : 'listas/Section0036.html',})
    .when('/c036', { templateUrl : 'listas/Section0037.html',})
    .when('/c037', { templateUrl : 'listas/Section0038.html',})
    .when('/c038', { templateUrl : 'listas/Section0039.html',})
    .when('/c039', { templateUrl : 'listas/Section0040.html',})
    .when('/c040', { templateUrl : 'listas/Section0041.html',})

    .when('/c041', { templateUrl : 'listas/Section0042.html',})
    .when('/c042', { templateUrl : 'listas/Section0043.html',})
    .when('/c043', { templateUrl : 'listas/Section0044.html',})
    .when('/c044', { templateUrl : 'listas/Section0045.html',})
    .when('/c045', { templateUrl : 'listas/Section0046.html',})
    .when('/c046', { templateUrl : 'listas/Section0047.html',})
    .when('/c047', { templateUrl : 'listas/Section0048.html',})
    .when('/c048', { templateUrl : 'listas/Section0049.html',})
    .when('/c049', { templateUrl : 'listas/Section0050.html',})
    .when('/c050', { templateUrl : 'listas/Section0051.html',})

    .when('/c051', { templateUrl : 'listas/Section0052.html',})
    .when('/c052', { templateUrl : 'listas/Section0053.html',})
    .when('/c053', { templateUrl : 'listas/Section0054.html',})
    .when('/c054', { templateUrl : 'listas/Section0055.html',})
    .when('/c055', { templateUrl : 'listas/Section0056.html',})
    .when('/c056', { templateUrl : 'listas/Section0057.html',})
    .when('/c057', { templateUrl : 'listas/Section0058.html',})
    .when('/c058', { templateUrl : 'listas/Section0059.html',})
    .when('/c059', { templateUrl : 'listas/Section0060.html',})
    .when('/c060', { templateUrl : 'listas/Section0061.html',})

    .when('/c061', { templateUrl : 'listas/Section0062.html',})
    .when('/c062', { templateUrl : 'listas/Section0063.html',})
    .when('/c063', { templateUrl : 'listas/Section0064.html',})
    .when('/c064', { templateUrl : 'listas/Section0065.html',})
    .when('/c065', { templateUrl : 'listas/Section0066.html',})
    .when('/c066', { templateUrl : 'listas/Section0067.html',})
    .when('/c067', { templateUrl : 'listas/Section0068.html',})
    .when('/c068', { templateUrl : 'listas/Section0069.html',})
    .when('/c069', { templateUrl : 'listas/Section0070.html',})
    .when('/c070', { templateUrl : 'listas/Section0071.html',})

    .when('/c071', { templateUrl : 'listas/Section0072.html',})
    .when('/c072', { templateUrl : 'listas/Section0073.html',})
    .when('/c073', { templateUrl : 'listas/Section0074.html',})
    .when('/c074', { templateUrl : 'listas/Section0075.html',})
    .when('/c075', { templateUrl : 'listas/Section0076.html',})
    .when('/c076', { templateUrl : 'listas/Section0077.html',})
    .when('/c077', { templateUrl : 'listas/Section0078.html',})
    .when('/c078', { templateUrl : 'listas/Section0079.html',})
    .when('/c079', { templateUrl : 'listas/Section0080.html',})
    .when('/c080', { templateUrl : 'listas/Section0081.html',})

    .when('/c081', { templateUrl : 'listas/Section0082.html',})
    .when('/c082', { templateUrl : 'listas/Section0083.html',})
    .when('/c083', { templateUrl : 'listas/Section0084.html',})
    .when('/c084', { templateUrl : 'listas/Section0085.html',})
    .when('/c085', { templateUrl : 'listas/Section0086.html',})
    .when('/c086', { templateUrl : 'listas/Section0087.html',})
    .when('/c087', { templateUrl : 'listas/Section0088.html',})
    .when('/c088', { templateUrl : 'listas/Section0089.html',})
    .when('/c089', { templateUrl : 'listas/Section0090.html',})
    .when('/c090', { templateUrl : 'listas/Section0091.html',})

    .when('/c091', { templateUrl : 'listas/Section0092.html',})
    .when('/c092', { templateUrl : 'listas/Section0093.html',})
    .when('/c093', { templateUrl : 'listas/Section0094.html',})
    .when('/c094', { templateUrl : 'listas/Section0095.html',})
    .when('/c095', { templateUrl : 'listas/Section0096.html',})
    .when('/c096', { templateUrl : 'listas/Section0097.html',})
    .when('/c097', { templateUrl : 'listas/Section0098.html',})
    .when('/c098', { templateUrl : 'listas/Section0099.html',})
    .when('/c099', { templateUrl : 'listas/Section0100.html',})
    .when('/c100', { templateUrl : 'listas/Section0101.html',})

    .when('/c101', { templateUrl : 'listas/Section0102.html',})
    .when('/c102', { templateUrl : 'listas/Section0103.html',})
    .when('/c103', { templateUrl : 'listas/Section0104.html',})
    .when('/c104', { templateUrl : 'listas/Section0105.html',})
    .when('/c105', { templateUrl : 'listas/Section0106.html',})
    .when('/c106', { templateUrl : 'listas/Section0107.html',})
    .when('/c107', { templateUrl : 'listas/Section0108.html',})
    .when('/c108', { templateUrl : 'listas/Section0109.html',})
    .when('/c109', { templateUrl : 'listas/Section0110.html',})
    .when('/c110', { templateUrl : 'listas/Section0111.html',})

    .when('/c111', { templateUrl : 'listas/Section0112.html',})
    .when('/c112', { templateUrl : 'listas/Section0113.html',})
    .when('/c113', { templateUrl : 'listas/Section0114.html',})
    .when('/c114', { templateUrl : 'listas/Section0115.html',})
    .when('/c115', { templateUrl : 'listas/Section0116.html',})
    .when('/c116', { templateUrl : 'listas/Section0117.html',})
    .when('/c117', { templateUrl : 'listas/Section0118.html',})
    .when('/c118', { templateUrl : 'listas/Section0119.html',})
    .when('/c119', { templateUrl : 'listas/Section0120.html',})
    .when('/c120', { templateUrl : 'listas/Section0121.html',})

    .when('/c121', { templateUrl : 'listas/Section0122.html',})
    .when('/c122', { templateUrl : 'listas/Section0123.html',})
    .when('/c123', { templateUrl : 'listas/Section0124.html',})
    .when('/c124', { templateUrl : 'listas/Section0125.html',})
    .when('/c125', { templateUrl : 'listas/Section0126.html',})
    .when('/c126', { templateUrl : 'listas/Section0127.html',})
    .when('/c127', { templateUrl : 'listas/Section0128.html',})
    .when('/c128', { templateUrl : 'listas/Section0129.html',})
    .when('/c129', { templateUrl : 'listas/Section0130.html',})
    .when('/c130', { templateUrl : 'listas/Section0131.html',})

    .when('/c131', { templateUrl : 'listas/Section0132.html',})
    .when('/c132', { templateUrl : 'listas/Section0133.html',})
    .when('/c133', { templateUrl : 'listas/Section0134.html',})
    .when('/c134', { templateUrl : 'listas/Section0135.html',})
    .when('/c135', { templateUrl : 'listas/Section0136.html',})
    .when('/c136', { templateUrl : 'listas/Section0137.html',})
    .when('/c137', { templateUrl : 'listas/Section0138.html',})
    .when('/c138', { templateUrl : 'listas/Section0139.html',})
    
    
		.otherwise({
			redirectTo: '/'
		});
});


angularRoutingApp.controller('mainController', function($scope) {
    
    $scope.pixeles1 = 16;
});

angularRoutingApp.controller('aboutController', function($scope) {
    
});

angularRoutingApp.controller('contactController', function($scope) {
    
});

//		angularRoutingApp.controller('fontSize', [ '$scope', function ($scope) {
//    
//    		$scope.pixeles = 16;
//    
//		}]);

// input type range

var range = $('.input-range'),
    value = $('.range-value');
    
value.html(range.attr('value'));

range.on('input', function(){
    value.html(this.value);
});