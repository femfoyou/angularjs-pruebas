// Sidenav collapse mobile

(function($){
  $(function(){

    $('.button-collapse').sideNav();

  }); // end of document ready
})(jQuery); // end of jQuery name space


// Slider

$(document).ready(function(){
      $('.slider').slider({
          full_width: true,
          height: 500,
        // Transition: 500,
          indicators: false,
          
      });
    });


// TABS

$(document).ready(function(){
            $('ul.tabs').tabs();
});